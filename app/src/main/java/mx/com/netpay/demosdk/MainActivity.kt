package mx.com.netpay.demosdk

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.netpay.demosdk.R

import kotlinx.android.synthetic.main.activity_main.*
import mx.com.netpay.sdk.IPage
import mx.com.netpay.sdk.IPage.ILine.IUnit.Companion.BOLD
import mx.com.netpay.sdk.SmartApiFactory
import mx.com.netpay.sdk.exceptions.SmartApiException
import mx.com.netpay.sdk.models.*

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener{

    private val smartApi = SmartApiFactory.createSmartApi(this)

    private var enableStatusBar = true
    private var showNavBar = true
    private var showStatBar = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sv.requestFocus()
        keyOptions.adapter = ArrayAdapter<NavBarButtons>(this, R.layout.support_simple_spinner_dropdown_item, NavBarButtons.values())

        keyOptions.onItemSelectedListener = this

        ArrayAdapter.createFromResource(this, R.array.key_enablers, R.layout.support_simple_spinner_dropdown_item)
            .also{adapter ->
                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                keyEnabler.adapter = adapter
            }

        keyEnabler.onItemSelectedListener = this

        if(!enableStatusBar){
            statusButton.text = "Habilitar barra de estado"
        }
        else{
            statusButton.text = "Deshabilitar barra de estado"
        }

        saleButton.setOnClickListener {
            val folio = folio.text.toString()
            val amount = amountTE.text.toString().toDoubleOrNull()?:0.0
            val tip = tip.text.toString().toDoubleOrNull()?:0.0
            val sale = SaleRequest("mx.com.netpay.demosdk", amount, folio = folio,tip = tip)
            try {
                smartApi.doTrans(sale)
            } catch (e: SmartApiException) {
                Toast.makeText(this, e.message,Toast.LENGTH_LONG).show()
            }
        }

        btn_sale_storeId.setOnClickListener {
            val folio = folio.text.toString()
            val amount = amountTE.text.toString().toDoubleOrNull()?:0.0
            val storeId = et_storeId.text.toString()
            val sale = SaleRequest("mx.com.netpay.demosdk", amount, folio = folio)
            try {
                smartApi.doTrans(sale)
            } catch (e: SmartApiException) {
                Toast.makeText(this, e.message,Toast.LENGTH_LONG).show()
            }
        }

        saleButtonAdditionalData.setOnClickListener {


            val amount = amountTE.text.toString().toDoubleOrNull()?:0.0
            val sale = SaleRequest("mx.com.netpay.demosdk", amount)
            try {
                smartApi.doTrans(sale,createPage())
            }catch (e: SmartApiException){
                Toast.makeText(this, e.message,Toast.LENGTH_LONG).show()
            }
        }

        saleButtonAdditionalBitmap.setOnClickListener {
            val amount = amountTE.text.toString().toDoubleOrNull()?:0.0
            val sale = SaleRequest("mx.com.netpay.demosdk", amount)
            try {
                smartApi.doTrans(sale,logo = logoBitmap())
            }catch (e: SmartApiException){
                Toast.makeText(this, e.message,Toast.LENGTH_LONG).show()
            }

        }

        saleButtonAdditionalDataAndBitmap.setOnClickListener {
            val amount = amountTE.text.toString().toDoubleOrNull()?:0.0
            val sale = SaleRequest("mx.com.netpay.demosdk", amount)
            try {
                smartApi.doTrans(sale,createPage(),logoBitmap())
            }catch (e: SmartApiException){
                Toast.makeText(this, e.message,Toast.LENGTH_LONG).show()
            }
        }

        reprintButton.setOnClickListener {
            val orderId = orderIdTE.text.toString()
            val storeId = et_storeId.text.toString()
            val sale = ReprintRequest("mx.com.netpay.demosdk", orderId )
            try {
                smartApi.doTrans(sale)
            } catch (e: SmartApiException) {
                Toast.makeText(this, e.message,Toast.LENGTH_LONG).show()
            }
        }

        reprintButtonExtras.setOnClickListener {
            val orderId = orderIdTE.text.toString()
            val storeId = et_storeId.text.toString()
            val sale = ReprintRequest("mx.com.netpay.demosdk", orderId )
            try {
                smartApi.doTrans(sale, createLongPage(20))
            } catch (e: SmartApiException) {
                Toast.makeText(this, e.message,Toast.LENGTH_LONG).show()
            }
        }

        cierreLoteButton.setOnClickListener {
            smartApi.doSettlement(
                SettlementRequest(
                    "mx.com.netpay.demosdk",
                    orderIdTE.text.toString()
                )
            )
        }

        clearButton.setOnClickListener {
            tvResponse.text = ""
        }

        voidButton.setOnClickListener {
            val orderId = orderIdTE.text.toString()
            val storeId = et_storeId.text.toString()
            val sale = CancelRequest("mx.com.netpay.demosdk", orderId )
            try {
                smartApi.doTrans(sale)
            } catch (e: SmartApiException) {
                Toast.makeText(this, e.message,Toast.LENGTH_LONG).show()
            }
        }

        printButton.setOnClickListener {
            val lines : Int = et_lines.text.toString().toIntOrNull()?:0
            val print = PrintRequest("mx.com.netpay.demosdk", createLongPage(lines) )
            try {
                smartApi.doTrans(print)
            } catch (e: SmartApiException) {
                Toast.makeText(this, e.message,Toast.LENGTH_LONG).show()
            }
        }

        checkStatus.setOnClickListener {
            val folio = folio.text.toString()
            val checkStatus = CheckStatusRequest("mx.com.netpay.demosdk", folio)
            try {
                smartApi.doTrans(checkStatus)
            } catch (e: SmartApiException) {
                Toast.makeText(this, e.message,Toast.LENGTH_LONG).show()
            }
        }

        enableButton.setOnClickListener {
            val navButtonSelected = keyOptions.selectedItem as NavBarButtons
            val status = keyEnabler.selectedItemPosition != 0

            //Toast.makeText(applicationContext, status.toString(), Toast.LENGTH_SHORT).show()

            smartApi.enableNavigation(navButtonSelected, status)

        }

        statusButton.setOnClickListener {
            enableStatusBar = !enableStatusBar
            smartApi.enableStatusBar(enableStatusBar)
            if(!enableStatusBar){
                statusButton.text = "Habilitar barra de estado"
            }
            else{
                statusButton.text = "Deshabilitar barra de estado"
            }
        }

        showNavButton.setOnClickListener {
            showNavBar = !showNavBar
            smartApi.showNavBar(showNavBar)
        }

        showStatButton.setOnClickListener {
            showStatBar = !showStatBar
            smartApi.showStatusBar(showStatBar)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(data != null) {
            val response = when(requestCode) {
                Constants.SALE_REQUEST -> smartApi.onResult(requestCode,resultCode,data) as SaleResponse
                Constants.CANCEL_REQUEST -> smartApi.onResult(requestCode,resultCode,data) as CancelResponse
                Constants.REPRINT_REQUEST-> smartApi.onResult(requestCode,resultCode,data) as ReprintResponse
                Constants.PRINT_REQUEST-> smartApi.onResult(requestCode,resultCode,data) as PrintResponse
                Constants.NAVIGATION_REQUEST -> smartApi.onResult(requestCode, resultCode, data)
                Constants.STATUS_REQUEST -> smartApi.onResult(requestCode, resultCode, data)
                Constants.SHOW_NAVBAR_REQUEST -> smartApi.onResult(requestCode, resultCode, data)
                Constants.SHOW_STATBAR_REQUEST -> smartApi.onResult(requestCode, resultCode, data)
                Constants.CHECK_STATUS_REQUEST-> smartApi.onResult(requestCode,resultCode,data) as CheckStatusResponse
                Constants.SETTLEMENT_REQUEST-> smartApi.onResult(requestCode,resultCode,data)
                else -> null
            }
            tvResponse.text = Gson().toJson(response)
        }
    }


    private fun createPage():IPage{
        val page = smartApi.createPage()
     /*   page.addLine()
            .addUnit(
                page.createUnit().apply {
                    text = "EXAMPLE  DATA"
                    textStyle = BOLD
                    fontSize = 24
                    gravity = Gravity.CENTER
                }
            )

        page.addLine()
            .addUnit(
                page.createUnit().apply {
                    text = "ADDITIONAL DATA "
                    textStyle = BOLD
                    fontSize = 24
                    gravity = Gravity.CENTER
                }

            )

            .addUnit(
                page.createUnit().apply {
                    text = "EXAMPLE 3"
                    textStyle = BOLD
                    fontSize = 24
                    gravity = Gravity.END
                }

            )*/
        page.addLine()
            .addUnit(
                page.createUnit().apply {
                    bitmap= logoBitmap()
                }
            )

      /*  page.addLine()
            .addUnit(
                page.createUnit().apply {
                    text = "EXAMPLE  DATA AFTER Logo"

                }
            )*/
        return page
    }

    private fun createLongPage(lines: Int = 60): IPage{
        val page = smartApi.createPage()

        if(lines > 50 ){
            page.addLine().addUnit(page.createUnit().apply {
                text = "-----------------------------"
                gravity = Gravity.CENTER
            })

            page.addLine().addUnit(page.createUnit().apply {
                text = ""
                bitmap = logoBitmap()
                gravity = Gravity.CENTER
            })
        }

        page.addLine()
            .addUnit(
                page.createUnit().apply {
                    text = "EXAMPLE  DATA"
                    textStyle = BOLD
                    fontSize = 24
                    gravity = Gravity.CENTER
                }
            )

        page.addLine()
            .addUnit(
                page.createUnit().apply {
                    text = "ADDITIONAL DATA "
                    textStyle = BOLD
                    fontSize = 24
                    gravity = Gravity.CENTER
                }

            )

            .addUnit(
                page.createUnit().apply {
                    text = "EXAMPLE 3"
                    textStyle = BOLD
                    fontSize = 24
                    gravity = Gravity.END
                }

            )
        for(i in 0 until lines ){
            page.addLine()
                .addUnit(page.createUnit().apply {
                    text = "LOREM IPSUM DOLOR SIT LINE ${i+1}"
                    gravity = Gravity.START
                })
        }
        return page
    }

    private fun logoBitmap():Bitmap{
        return BitmapFactory.decodeResource(resources, R.drawable.logo_ticket)
        //val myDrawable = getDrawable(R.drawable.logo_ticket)
        //return (myDrawable as BitmapDrawable).bitmap
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if(keyEnabler.selectedItemPosition == 0){
            enableButton.text = "Deshabilitar botón"
        }
        else{
            enableButton.text = "Habilitar botón"
        }
    }
}
