package mx.com.netpay.demosdk;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.google.gson.Gson;
import com.netpay.demosdk.R;
import mx.com.netpay.sdk.IPage;
import mx.com.netpay.sdk.SmartApi;
import mx.com.netpay.sdk.SmartApiFactory;
import mx.com.netpay.sdk.models.*;

public class MainActivityJava extends AppCompatActivity {

    private SmartApi smartApi = SmartApiFactory.INSTANCE.createSmartApi(this);

    private TextView responseTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button sale = findViewById(R.id.saleButton);
        Button cancel = findViewById(R.id.voidButton);
        Button reprint = findViewById(R.id.reprintButton);
        Button print = findViewById(R.id.printButton);
        Button clear = findViewById(R.id.clearButton);

        final EditText amountEt = findViewById(R.id.amountTE);
        final EditText tip = findViewById(R.id.tip);
        final EditText orderIdEt = findViewById(R.id.orderIdTE);

        responseTv = findViewById(R.id.tvResponse);

        sale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double amount = Double.parseDouble(amountEt.getText().toString());

                double tip =Double.parseDouble(amountEt.getText().toString());

                SaleRequest sale = new SaleRequest("mx.com.netpay.demosdk", amount, tip,null, null, null,
                        null, null, null, null, null, null, null,
                        null, null, "");
                smartApi.doTrans(sale);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String orderId = orderIdEt.getText().toString();
                CancelRequest voidRequest = new CancelRequest("mx.com.netpay.demosdk", orderId, "");
                smartApi.doTrans(voidRequest);
            }
        });

        reprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String orderId = orderIdEt.getText().toString();
                ReprintRequest reprintRequest = new ReprintRequest("mx.com.netpay.demosdk", orderId, "");
                smartApi.doTrans(reprintRequest);
            }
        });

        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Crear una página
                IPage page = smartApi.createPage();

                //Crear unidad que contiene texto y otros formatos
                IPage.ILine.IUnit unit1 = page.createUnit();
                unit1.setText("Texto 1");
                unit1.setGravity(Gravity.START);

                //Se pueden agregar 2 o más unidades a una línea y se dividirá en columnas
                IPage.ILine.IUnit unit2 = page.createUnit();
                unit2.setText("Texto 2");
                unit2.setGravity(Gravity.END);

                //Se crea una línea y se agregan sus unidades.
                page.addLine().
                        addUnit(unit1).
                        addUnit(unit2);

                //Se crea una nueva unidad
                IPage.ILine.IUnit unit3 = page.createUnit();
                unit3.setText("Texto 3");
                unit3.setGravity(Gravity.CENTER);

                //Se crea una nueva línea y se agrega la unidad pasada
                page.addLine().addUnit(unit3);

                //Se crea un request del tipo PrintRequest con el package name del app y la página creada
                PrintRequest printRequest = new PrintRequest("mx.com.netpay.demosdk", page);

                smartApi.doTrans(printRequest);
            }
        });


        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                responseTv.setText("");
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null) {
            BaseResponse response = null;
            switch (requestCode) {
                case Constants.SALE_REQUEST:
                    response = (SaleResponse) smartApi.onResult(requestCode,resultCode,data);
                    break;

                case Constants.CANCEL_REQUEST:
                    response = (CancelResponse) smartApi.onResult(requestCode,resultCode,data);
                    break;

                case Constants.REPRINT_REQUEST:
                    response = (ReprintResponse) smartApi.onResult(requestCode,resultCode,data);

                    break;

                case Constants.PRINT_REQUEST:
                    response = (PrintResponse) smartApi.onResult(requestCode,resultCode,data);
                    break;
            }
            if(response != null) {
                responseTv.setText(new Gson().toJson(response));
            }
        }
    }
}
